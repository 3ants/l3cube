#Enter directory path as an argument

import os
import hashlib, shutil
import sys
from collections import defaultdict
from stat import *
all_files = []
BLOCKSIZE = 65536

def hashfile(path):
	afile = open(path, 'rb')
	hasher = hashlib.sha1()
	buf = afile.read(BLOCKSIZE)
	while len(buf) > 0:
		hasher.update(buf)
		buf = afile.read(BLOCKSIZE)
	afile.close()
	return hasher.hexdigest()


for root, directories, filenames in os.walk(sys.argv[1]):
	for directory in directories:
		os.path.join(root, directory) 
	for filename in filenames:
		try:
			pathname = os.path.join(root, filename)
			mode = os.stat(pathname).st_mode
			if S_ISREG(mode):
				all_files.append(pathname)
		except:
			pass

size = defaultdict(list)
for file1 in all_files:
	if(os.path.exists(file1)):
		size[os.path.getsize(file1)].append(file1)
for s in size.items():
	if(len(s[1]) > 1 ):
		hashlist = defaultdict(list)
		print "calculating for : ========================="
		for each_file in s[1]:
			hasher = hashlib.sha1()
			try:
				filehash = hashfile(each_file)
				print each_file
				hashlist[filehash].append(each_file)
			except:
				pass

		for h in hashlist.items():
			if(len(h[1]) > 1):
				print "Duplicate files:"
				print h[1]
				ch = raw_input("Do you want to delete(1), merge(2) or nothing(3)? ")
				if(ch == "1"):
					for f in h[1]:
						ch1 = raw_input("Do you want to delete "+ f +" (y/n) ")
						if(ch1 == "y" or ch1 == "Y"):
							os.remove(f)
				elif(ch == "2"):
					while(True):
						path = raw_input("Enter new valid file path for merging files: ")
						if(os.path.exists(path)):
							print "File already exists!!"
						else:
							f1 = open(path,"w")							
							print h[1][0]
							shutil.copyfile(h[1][0],path)
							f1.close()
							for f in h[1]:
								os.remove(f)
							break
				else:
					print "Continuing......"
