#Enter directory name as argument

import os
import hashlib, shutil
import sys
BLOCKSIZE = 65536
from collections import defaultdict
all_files=[]
for root, directories, filenames in os.walk(sys.argv[1]):
	for directory in directories:
		os.path.join(root, directory) 
	for filename in filenames:	
		all_files.append(os.path.join(root,filename))

size = defaultdict(list)
for file1 in all_files:
	size[os.path.getsize(file1)].append(file1)
for s in size.items():
	if(len(s[1])> 1 ):
		hashlist=defaultdict(list)
		for each_file in s[1]:
			hasher = hashlib.sha1()
			with open(each_file, 'rb') as afile:
    				buf = afile.read(BLOCKSIZE)
    				while len(buf) > 0:
        				hasher.update(buf)
        				buf = afile.read(BLOCKSIZE)
					filehash = hasher.hexdigest()
					hashlist[filehash].append(each_file)
    		afile.close()
		for h in hashlist.items():
			if(len(h[1])>1):
				print "Duplicate files:"
				print h[1]
				ch=raw_input("Do you want to delete(1) or merge(2)?")
				if(ch=="1"):
					for f in h[1]:
						ch1=raw_input("Do you want to delete "+ f +" (y/n)")
						if(ch1=="y" or ch1=="Y"):
							os.remove(f)
				elif(ch=="2"):
					path=raw_input("Enter new file path for merging files:")
					if(os.path.exists(path)):
						print "File already exists!!"
					else:
						f1=open(path,"w")
					print h[1][0]
					shutil.copyfile(h[1][0],path)
					f1.close()
					for f in h[1]:
						os.remove(f)






































