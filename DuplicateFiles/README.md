Duplicate Files  
==============================================

This program lists duplicate files from hard drive and gives user option to remove them or merge them. It accepts the directory name as a command-line argument.
In the first version we have used md5 hash to find duplicate files. But, md5 is broken, it may generate collision, so md5 should not be used for finding duplicate files. In second version, we have used SHA1. SHA1 is not broken and is believed to be secure.
This program finds all the files and checks for the files whose size is same. It groups files according to their size. Now, only files with same sizes are compared to check if hash value of these files is same or not. Optimization is done as we are not calculating hash value for every file, only for those whose size is same.
	
Advantages of SHA1 over md5 :
1. SHA1 has larger state : 160 bits. 
   md5 has 128 bits.
2. SHA1 has 80 rounds.
   md5 has 64 rounds.


Usage Info:
------------------------------------------------

python sha1_duplicate.py <directory-path>                       


Authors :
------------------------------------------------

1) Pooja Patil
2) Mrinmayi Kinare
3) Swapnil Khandekar


Bug Report:
------------------------------------------------

You can submit your bugs here: swa.khandekar@gmail.com




  
