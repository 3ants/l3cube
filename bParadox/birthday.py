'''
Birthday Paradox  
This program allocates random birthdays to the people using the 
random integer generator function. It calculates probability such that, in a set of n
randomly chosen people at least one pair of them will have same birthday.
where, 0 < n <101.
It then plots a graph showing Computed probability and Number of people

'''


from random import randint
import matplotlib.pyplot as plt

print "no of people probability"
xt = list()
yt = list()
for k in range(100):
	prob = 0
	for j in range(10000):
		count = [0]*366
		for i in range(k):
			temp = randint(1,365)
			if(count[temp]):
				prob += 1;
				break;
			count[temp] = 1;
		
		
	ans = float(prob)/10000
	print k, "            ", ans 
	xt.append(k)
	yt.append(ans)
plt.xlabel("Number of people")
plt.ylabel("Probablity")
lt=plt.plot(xt,yt)
plt.setp(lt,color='b')
plt.show()




'''

***************OUTPUT**************************************

no of people probability
0              0.0
1              0.0
2              0.0034
3              0.0087
4              0.0172
5              0.0277
6              0.0387
7              0.0554
8              0.0731
9              0.0922
10              0.1118
11              0.1406
12              0.1683
13              0.2002
14              0.223
15              0.2551
16              0.2865
17              0.3231
18              0.3464
19              0.3785
20              0.4154
21              0.4473
22              0.4759
23              0.5
24              0.54
25              0.5666
26              0.5986
27              0.6233
28              0.6613
29              0.6831
30              0.7022
31              0.7309
32              0.754
33              0.7755
34              0.7903
35              0.8214
36              0.8321
37              0.8437
38              0.8635
39              0.8764
40              0.8934
41              0.9055
42              0.9151
43              0.9243
44              0.9362
45              0.9359
46              0.9495
47              0.9555
48              0.9609
49              0.9684
50              0.9702
51              0.9734
52              0.9782
53              0.9804
54              0.9836
55              0.9854
56              0.9893
57              0.9907
58              0.9903
59              0.9927
60              0.9939
61              0.9946
62              0.9959
63              0.9974
64              0.9973
65              0.9973
66              0.9985
67              0.999
68              0.9986
69              0.9984
70              0.9992
71              0.9994
72              0.9997
73              0.9996
74              0.9995
75              0.9995
76              0.9997
77              0.9999
78              0.9998
79              0.9999
80              0.9998
81              1.0
82              0.9999
83              0.9999
84              0.9999
85              1.0
86              1.0
87              1.0
88              1.0
89              1.0
90              1.0
91              1.0
92              1.0
93              1.0
94              1.0
95              1.0
96              1.0
97              1.0
98              1.0
99              1.0


'''
