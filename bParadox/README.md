BParadox
====================================

    BParadox is used to test the validity of the classic Birthday Paradox.
For this we generate about 10000 random experiments for each of n number of people to be 
considered for the Birthday Problem.
    Output of this is a graph plotted of probabilities of people having same birthdays vs
number of people considered.
    Out of all tests performed we could match the theoritcal probabilty for 23 people as 0.5 .
And hence we conclude that Birthday Paradox is true.

Usage info:
------------------------------------

Go to the source folder and run

$> python birthday.py

Dependencies:
--------------------------------------

1. Python matplotlib library.

Authors:
--------------------------------------

1) Pooja Patil
2) Mrinmayi Kinare
3) Swapnil Khandekar

Contact:
---------------------------------------
For any queries contact poojavp95@gmail.com




