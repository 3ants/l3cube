/*
 * inode.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: swapnil
 */

#include "inode.h"

inode::inode(string f_name)
{
	file_size = -1;
	start_position = -1;
	file_name_length = f_name.size();
	file_name = new char[file_name_length];
	strcpy(file_name, f_name.c_str());
	next_inode_addr = -1;
}

inode::~inode() {
	// TODO Auto-generated destructor stub
}

int inode::get_size()
{
	return 4 * sizeof(int) + file_name_length;
}
