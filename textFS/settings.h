/*
 * settings.h
 *
 *  Created on: Jun 1, 2016
 *      Author: swapnil
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

const char* HOME_DIR;
string TEXT_FS;
string TEXT_FS_PATH;

void get_home_dir()
{
	if ((HOME_DIR = getenv("HOME")) == NULL) {
	    HOME_DIR = getpwuid(getuid())->pw_dir;
	}
}

#endif /* SETTINGS_H_ */
