/*
 * copy.cpp
 *
 *  Created on: 01-Jun-2016
 *      Author: pooja
 */
#include <bits/stdc++.h>
#include "inode.h"
#include "superblock.h"
#include "settings.h"

using namespace std;

super_block sblock;
string DEST_FILE;
string SOURCE_FILE;

bool init()
{
	get_home_dir(); 												/* inits HOME_DIR variable */
	TEXT_FS = "/.textFS";
	TEXT_FS_PATH = HOME_DIR + TEXT_FS;

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	if(!textFs.good())
	{
		cout<<"No TextFs found... Create new file in Textfs and then copy..\n";
		textFs.close();
		return 0;
	}
	textFs.close();
	return 1;
}
int main(int argc, char const *argv[])
{
	if(!init())
		return -1;
	if(argc < 3)
	{
		cout<<"Wrong Syntax"<<endl;
		cout<<"Usage: copy <destination in textfs> <source>"<<endl;
		return -1;
	}

	DEST_FILE = (string)argv[1];
	SOURCE_FILE = (string)argv[2];

	fstream src_file;
	src_file.open(SOURCE_FILE.c_str(),ios::in | ios::binary);
	if(!src_file.good())
	{
		cout<<SOURCE_FILE<<" does not exist.."<<endl;
		src_file.close();
		return -1;
	}

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::binary | ios::in | ios::out);

	int file_name_length;
	char *file_name;
	int start_position;
	int file_size;
	int next_inode_addr;

	int total_size;
	textFs.seekg(sizeof(unsigned int));
	textFs.read((char*)&total_size, sizeof(unsigned int));
	sblock.total_size = total_size;

	textFs.seekg(sblock.first_inode_addr);
	int length_file=0;
	while(textFs.tellg() < sblock.total_size)
	{
		textFs.read((char*)&file_name_length, sizeof(int));
		file_name = new char[file_name_length + 1];
		textFs.read(file_name, file_name_length);
		file_name[file_name_length] = '\0';

		textFs.read((char*)&start_position, sizeof(int));
		textFs.read((char*)&file_size, sizeof(int));
		textFs.read((char*)&next_inode_addr, sizeof(int));
		int pos = textFs.tellg();
		if(string(file_name) == DEST_FILE )
		{
			if(start_position != -1 and file_size != -1)
			{
				cout<<DEST_FILE<<" already has contents.Cannot copy "<<SOURCE_FILE<<" into it.";
				break;
			}
			else
			{
				src_file.seekg(0,ios::end);
				length_file = src_file.tellg();
				char buffer[length_file];
				
				src_file.seekg(0);									/* Copy contents into file */
				textFs.seekp(sblock.total_size);
				src_file.read(buffer, length_file);
				textFs.write(buffer, length_file);

				textFs.seekp(pos - (3 * sizeof(int)));					/* Update inode with start of file and file length */
				int start_position = sblock.total_size - pos;
				textFs.write((char*)&start_position, sizeof(int));
				textFs.write((char*)&length_file, sizeof(int));
				src_file.close();

				/* Winding up */
				sblock.total_size += length_file;
				textFs.seekp(sizeof(unsigned int));
				textFs.write((char*)&sblock.total_size, sizeof(unsigned int));
				cout<<"File copied successfully! New total size is: "<<sblock.total_size<<" bytes\n";
				textFs.close();
				break;
			}
		}

		if(next_inode_addr == -1)
		{
			cout<<DEST_FILE<<" not created...Create "<<DEST_FILE<<" first.";
			break;
		}

		textFs.seekg(next_inode_addr, ios_base::cur);              /* address where next inode starts.. 
																		next_inode_addr is offset 
																		from end of previous inode*/
	}
	textFs.close();
	return 0;

}




