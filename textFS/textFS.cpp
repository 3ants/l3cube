#include <bits/stdc++.h>

using namespace std;

int main()
{
	string command;
	while(1)
	{
		cout<<"Enter command: ";
		cin>>command;
		string run;
		if(command == "create")
		{
			string file_name;
			cin>>file_name;
			run = "./create " + file_name;
			system(run.c_str());
		}
		else if(command == "copy")
		{
			string dest_file_name,src_file_name;
			cin>>dest_file_name>>src_file_name;
			run = "./copy "+dest_file_name+" "+src_file_name;
			system(run.c_str());
		}
		else if(command == "list")
		{
			run = "./list";
			system(run.c_str());
		}
		else if(command == "echo")
		{
			string file_name;
			cin>>file_name;
			run = "./echo " + file_name;
			system(run.c_str());
		}
		else if(command == "delete")
		{
			string file_name;
			cin>>file_name;
			run = "./delete " + file_name;
			system(run.c_str());
		}
		else
		{
			cout<<"Invalid command\n";
		}
	}
	return 0;
}