/*
 * inode.h
 *
 *  Created on: Jun 1, 2016
 *      Author: swapnil
 */

#ifndef INODE_H_
#define INODE_H_
#include<bits/stdc++.h>
using namespace std;

class inode
{
public:
	inode(string);
	~inode();
	int get_size(); 				/* Returns size of this inode in bytes */

	int file_name_length;
	char *file_name;
	int start_position;  			/* end position = start postion + file size */
	int file_size;
	int next_inode_addr;			/* Relative to end of current inode end */
};



#endif /* INODE_H_ */
