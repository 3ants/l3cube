#include <bits/stdc++.h>
#include "inode.h"
#include "superblock.h"
#include "settings.h"


using namespace std;

super_block sblock;
string NEW_FILE_NAME;


bool init()
{
	get_home_dir(); 												/* inits HOME_DIR variable */
	TEXT_FS = "/.textFS";
	TEXT_FS_PATH = HOME_DIR + TEXT_FS;

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	if(!textFs.good())
	{
		cout<<"No TextFs found... Create new file first...\n";
		textFs.close();
		return 0;
	}
	else
	{
		int total_size;
		textFs.seekg(sizeof(unsigned int));
		textFs.read((char*)&total_size, sizeof(unsigned int));
		sblock.total_size = total_size;
		textFs.close();
	}
	
	return 1;
}


int main(int argc, char const *argv[])
{

	char *file_data;
	int file_name_length;
	char *file_name;
	int start_position;
	int file_size;
	int next_inode_addr;

	if(!init())
		return -1;

	if(argc < 2)
	{
		cout<<"Invalid number of arguments\n";
		cout<<"usage: echo <filename>\n";
	}

	NEW_FILE_NAME = (string)argv[1];
	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::binary | ios::in | ios::out);	
	textFs.seekg(sblock.first_inode_addr);

	while(textFs.tellg() < sblock.total_size)
	{
		textFs.read((char*)&file_name_length, sizeof(int));
		file_name = new char[file_name_length + 1];
		textFs.read(file_name, file_name_length);
		file_name[file_name_length] = '\0';
		textFs.read((char*)&start_position, sizeof(int));
		textFs.read((char*)&file_size, sizeof(int));
		textFs.read((char*)&next_inode_addr, sizeof(int));

		if(string(file_name) == NEW_FILE_NAME)
		{
			if(start_position != -1)
			{
				cout<<"Contents of the file '"<<NEW_FILE_NAME<<"' are : \n";
				textFs.seekg((long)textFs.tellg() + start_position);
				file_data = new char[file_size + 1];
				textFs.read(file_data, file_size);
				file_data[file_size] = '\0';
				cout<<file_data<<endl;
				return 0;
			}
			else
			{
				cout<<"Empty File !!!"<<endl;
				return 0;
			}
			textFs.close();
		}

		if(next_inode_addr == -1){
			cout<<"File does not exist ";
			break;
		}
			
		textFs.seekg((long)textFs.tellg() + next_inode_addr);              // address where next inode starts.. next_inode_addr is offset from end of first inode	
	}

	textFs.close();
	return 0;
}
