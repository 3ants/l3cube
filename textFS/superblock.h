/*
 * superblock.h
 *
 *  Created on: Jun 1, 2016
 *      Author: swapnil
 */

#ifndef SUPERBLOCK_H_
#define SUPERBLOCK_H_

class super_block {
public:
	super_block();
	virtual ~super_block();
	unsigned int first_inode_addr;
	unsigned int total_size;
};

#endif /* SUPERBLOCK_H_ */
