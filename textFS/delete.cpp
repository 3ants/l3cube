/*
 * delete.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: swapnil
 */

#include <bits/stdc++.h>
#include "inode.h"
#include "superblock.h"
#include "settings.h"

using namespace std;

super_block sblock;
int INODE_TO_BE_DELETED;
string FILE_TO_BE_DELETED;
int FILE_RELATIVE_ADDR;
int FILE_ASSOCIATED_SIZE;
int SIZE_OF_INODE;
int WRITE_FROM_THIS_LOCATION;
int READ_FROM_THIS_LOCATION;
int FILE_ABSOLUTE_ADDR;

bool init()
{
	get_home_dir(); 												/* inits HOME_DIR variable */
	TEXT_FS = "/.textFS";
	TEXT_FS_PATH = HOME_DIR + TEXT_FS;
	INODE_TO_BE_DELETED = -1;
	FILE_TO_BE_DELETED = "";
	FILE_RELATIVE_ADDR = -1;
	FILE_ASSOCIATED_SIZE = -1;

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	if(!textFs.good())
	{
		cout<<"No TextFs found. Exiting\n";
		textFs.close();
		return 0;
	}
	textFs.close();

	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	int total_size;
	textFs.seekg(sizeof(unsigned int));
	textFs.read((char*)&total_size, sizeof(unsigned int));
	sblock.total_size = total_size;
	textFs.close();
	return 1;
}

int main(int argc, char const *argv[])
{
	if(!init())
		return -1;

	if(argc < 2)
	{
		cout<<"Invalid no of arguments.\n";
		cout<<"usage: delete <name_of_file>\n";
		return -1;
	}

	FILE_TO_BE_DELETED = (string)argv[1];
	vector<inode> inode_vect;
	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::out | ios::binary);

	textFs.seekg(sblock.first_inode_addr);

	int file_name_length;
	char *file_name;

	textFs.seekg(sblock.first_inode_addr);

	while(textFs.tellg() < sblock.total_size)
	{
		textFs.read((char*)&file_name_length, sizeof(int));

		file_name = new char[file_name_length + 1];
		textFs.read(file_name, file_name_length);
		file_name[file_name_length] = '\0';

		inode i((string)file_name);

		textFs.read((char*)&i.start_position, sizeof(int));
		textFs.read((char*)&i.file_size, sizeof(int));
		textFs.read((char*)&i.next_inode_addr, sizeof(int));

		inode_vect.push_back(i);
		if((string)file_name == FILE_TO_BE_DELETED)
		{
			INODE_TO_BE_DELETED = inode_vect.size() - 1;
			//here pointer is at the end of inode to be deleted
			SIZE_OF_INODE = i.get_size();
			FILE_RELATIVE_ADDR = i.start_position;
			FILE_ASSOCIATED_SIZE = i.file_size;
			
			READ_FROM_THIS_LOCATION = textFs.tellg();
			WRITE_FROM_THIS_LOCATION = READ_FROM_THIS_LOCATION - i.get_size();
			FILE_ABSOLUTE_ADDR = READ_FROM_THIS_LOCATION + FILE_RELATIVE_ADDR;

			/*
			cout<<"SIZE_OF_INODE "<<SIZE_OF_INODE<<endl;
			cout<<"FILE_RELATIVE_ADDR "<<FILE_RELATIVE_ADDR<<endl;
			cout<<"FILE_ASSOCIATED_SIZE "<<FILE_ASSOCIATED_SIZE<<endl;
			
			cout<<"READ_FROM_THIS_LOCATION "<<READ_FROM_THIS_LOCATION<<endl;
			cout<<"WRITE_FROM_THIS_LOCATION "<<WRITE_FROM_THIS_LOCATION<<endl;
			cout<<"FILE_ABSOLUTE_ADDR "<<FILE_ABSOLUTE_ADDR<<endl;
			*/
		}
		if(i.next_inode_addr == -1)
			break;

		textFs.seekg(i.next_inode_addr, ios_base::cur);              /* address where next inode starts.. 
																		next_inode_addr is offset 
																		from end of previous inode*/
	}

	if(INODE_TO_BE_DELETED == -1)
	{
		cout<<"No such file in TextFs.\n";
		textFs.close();
		return -1;
	}

	vector <char> buffer;

	int start_position, file_size, next_inode_addr;
	textFs.seekg(sblock.first_inode_addr);
	int index = 0;
	while(textFs.tellg() < FILE_ABSOLUTE_ADDR)
	{
		textFs.read((char*)&file_name_length, sizeof(int));
		char *file_name = new char[file_name_length + 1];
		textFs.read(file_name, file_name_length);
		file_name[file_name_length] = '\0';

		textFs.read((char*)&start_position, sizeof(int));
		textFs.read((char*)&file_size, sizeof(int));
		textFs.read((char*)&next_inode_addr, sizeof(int));

		int CURRENT_INODE_END = textFs.tellg();
		int CURRENT_INODE_START = CURRENT_INODE_END - (4*sizeof(int) + file_name_length);
		int NOT_UPDATED_INODE = next_inode_addr;

		if(CURRENT_INODE_START < WRITE_FROM_THIS_LOCATION)
		{
			//file sathi
			if(start_position != -1)
			{
				if(FILE_RELATIVE_ADDR != -1 and start_position + CURRENT_INODE_END > FILE_ABSOLUTE_ADDR)
				{
					int update = start_position;
					//cout<<"for :"<<file_name<<" file addr update from "<<update;
					update -= (SIZE_OF_INODE + FILE_ASSOCIATED_SIZE);
					//cout<<"to "<<update<<endl;
					textFs.seekp(-3 * sizeof(int), ios_base::cur);
					textFs.write((char*)&update, sizeof(int));
					textFs.seekg(CURRENT_INODE_END);
				}
				else if(start_position + CURRENT_INODE_END >= READ_FROM_THIS_LOCATION)
				{
					int update = start_position;
					//cout<<"for :"<<file_name<<" file addr update from "<<update;
					update -= (SIZE_OF_INODE);
					//cout<<"to "<<update<<endl;
					textFs.seekp(-3 * sizeof(int), ios_base::cur);
					textFs.write((char*)&update, sizeof(int));
					textFs.seekg(CURRENT_INODE_END);
				}
			}

			//inode addr sathi

			if(next_inode_addr != -1)
			{
				if(index == INODE_TO_BE_DELETED - 1)
				{
					int addr;
					if(inode_vect[index + 1].next_inode_addr == -1)
						addr = -1;
					else
					{
						if(FILE_ABSOLUTE_ADDR != -1 and inode_vect[index + 1].next_inode_addr + READ_FROM_THIS_LOCATION > FILE_ABSOLUTE_ADDR)
							addr = next_inode_addr + inode_vect[index + 1].next_inode_addr - FILE_ASSOCIATED_SIZE;
						else
							addr = next_inode_addr + inode_vect[index + 1].next_inode_addr;
					}
					//cout<<"for :"<<file_name<<" next_inode_addr update from "<<next_inode_addr;
					//cout<<" to "<<addr<<endl;
					textFs.seekp(-1 * sizeof(int), ios_base::cur);
					textFs.write((char*)&addr, sizeof(int));
					textFs.seekp(CURRENT_INODE_END);
				}
				else if(FILE_RELATIVE_ADDR != -1 and next_inode_addr + CURRENT_INODE_END > FILE_ABSOLUTE_ADDR)
				{
					int update = next_inode_addr;
					//cout<<"for :"<<file_name<<" next_inode_addr update from "<<update;
					update -= (SIZE_OF_INODE + FILE_ASSOCIATED_SIZE);
					//cout<<"to "<<update<<endl;
					textFs.seekp(-1 * sizeof(int), ios_base::cur);
					textFs.write((char*)&update, sizeof(int));
					textFs.seekg(CURRENT_INODE_END);
				}
				else if(next_inode_addr + CURRENT_INODE_END >= READ_FROM_THIS_LOCATION)
				{
					int update = next_inode_addr;
					//cout<<"for :"<<file_name<<" next_inode_addr update from "<<update;
					update -= (SIZE_OF_INODE);
					//cout<<"to "<<update<<endl;
					textFs.seekp(-1 * sizeof(int), ios_base::cur);
					textFs.write((char*)&update, sizeof(int));
					textFs.seekg(CURRENT_INODE_END);	
				}
			}
		}
		else if(CURRENT_INODE_START > WRITE_FROM_THIS_LOCATION)
		{
			//file sathi
			if(start_position != -1)
			{
				if(start_position + CURRENT_INODE_END > FILE_ABSOLUTE_ADDR)
				{
					int update = start_position;
					//cout<<"for :"<<file_name<<" file addr update from "<<update;
					update -= (FILE_ASSOCIATED_SIZE);
					//cout<<"to "<<update<<endl;
					textFs.seekp(-3 * sizeof(int), ios_base::cur);
					textFs.write((char*)&update, sizeof(int));
					textFs.seekg(CURRENT_INODE_END);
				}
			}
			//inode sathi
			if(next_inode_addr != -1)
			{
				if(next_inode_addr + CURRENT_INODE_END > FILE_ABSOLUTE_ADDR)
				{
					int update = next_inode_addr;
					//cout<<"for :"<<file_name<<" next_inode_addr update from "<<update;
					update -= (FILE_ASSOCIATED_SIZE);
					//cout<<"to "<<update<<endl;
					textFs.seekp(-1 * sizeof(int), ios_base::cur);
					textFs.write((char*)&update, sizeof(int));
					textFs.seekg(CURRENT_INODE_END);
				}
			}
		}

		if(NOT_UPDATED_INODE == -1)
			break;
		
		index++;
		textFs.seekg(NOT_UPDATED_INODE, ios_base::cur);              /* address where next inode starts.. 
																		next_inode_addr is offset 
																		from end of previous inode*/
	}


	textFs.seekp(READ_FROM_THIS_LOCATION);
	while(textFs.tellg() != sblock.total_size)
	{
		if(FILE_RELATIVE_ADDR != -1 and textFs.tellg() == FILE_ABSOLUTE_ADDR)
		{
			//seek get pointer to end of file
			textFs.seekg(FILE_ASSOCIATED_SIZE, ios_base::cur);
		}
		else
		{
			char c;
			textFs.read((char*)&c, 1);
			buffer.push_back(c);
		}
	}

	//now we have all the data without data to be deleted in buffer

	textFs.seekp(0);
	textFs.seekp(WRITE_FROM_THIS_LOCATION);

	for (int i = 0; i < buffer.size(); ++i)
	{
		textFs.write((char*)&buffer[i], 1);
	}

	//finalize
	int size = SIZE_OF_INODE;
	if(FILE_ASSOCIATED_SIZE != -1)
		size += FILE_ASSOCIATED_SIZE;
	sblock.total_size -= size;
	textFs.seekp(sizeof(unsigned int));
	textFs.write((char*)&sblock.total_size, sizeof(unsigned int));
	cout<<"File deleted successfully! New total size is: "<<sblock.total_size<<" bytes\n";

	textFs.close();
	return 0;
}
