TextFS - A Secure Text File System
========================================================

Modules:
------------------------------------------------------
1) Create
2) Copy
3) List
4) Echo
5) Delete

TextFS is a secure file system that keeps your text files safe. You can use TextFS to hide your
personal text files. You can create new file in TextFS using create command. To copy your text file 
in TextFS file you can use copy command. Similarly using list command you can view list af all files,
 using echo command you can see contents, and using delete command you can delete any file in your TextFS.

Usage info:
-----------------------------------------------------
1) To create new file:
	./create <name_of_file>

2) To copy a file into TextFS:
	./copy <destination_file_name> <source_file_address>

3) To list all files in TextFS
	./list

4) To see contents of a file:
	./echo <name_of_file>

5) To delete a file:
	./delete <name_of_file>


Installation steps:
---------------------------------------------------------
1) Change your working directory to source code directory

2) run make command

Authors:
---------------------------------------------------------
1) Pooja Patil
2) Mrinmayi Kinare
3) Swapnil Khandekar

Bug Report:
---------------------------------------------------------
You can submit your bugs here: swa.khandekar@gmail.com
 
