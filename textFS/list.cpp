#include <bits/stdc++.h>
#include "inode.h"
#include "superblock.h"
#include "settings.h"

using namespace std;

super_block sblock;
string NEW_FILE_NAME;

bool init()
{
	get_home_dir(); 												/* inits HOME_DIR variable */
	TEXT_FS = "/.textFS";
	TEXT_FS_PATH = HOME_DIR + TEXT_FS;

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	if(!textFs.good())
	{
		cout<<"No TextFs found... Create new file first...\n";
		textFs.close();
		return 0;
	}
	else
	{
		int total_size;
		textFs.seekg(sizeof(unsigned int));
		textFs.read((char*)&total_size, sizeof(unsigned int));
		textFs.close();
		sblock.total_size = total_size;
	}
	textFs.close();
	return 1;
}

int main(int argc, char const *argv[])
{
	int file_name_length;	
	vector<inode> inode_list;

	if(!init())
		return -1;

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::binary | ios::in | ios::out);	
	textFs.seekg(sblock.first_inode_addr);

	cout<<"List of Private Files : "<<endl;
	while(textFs.tellg() < sblock.total_size)
	{
		textFs.read((char*)&file_name_length, sizeof(int));
		char *file_name = new char[file_name_length + 1];
		textFs.read(file_name, file_name_length);
		file_name[file_name_length] = '\0';
		
		inode i((string)file_name);

		textFs.read((char*)&i.start_position, sizeof(int));
		textFs.read((char*)&i.file_size, sizeof(int));
		textFs.read((char*)&i.next_inode_addr, sizeof(int));

		inode_list.push_back(i);

		if(i.next_inode_addr == -1)
			break;
	
		textFs.seekg(i.next_inode_addr, ios_base::cur);              /* address where next inode starts.. 
																		next_inode_addr is offset 
																		from end of previous inode*/
	}
	cout<<"Total file_size: "<<sblock.total_size<<endl;
	cout<<"Name\tSize (bytes)\n";
	for(int i = 0; i < inode_list.size(); i++){
		cout<<inode_list[i].file_name<<"\t";
		if(inode_list[i].file_size == -1)
			cout<<0<<endl;
		else
			cout<<inode_list[i].file_size + inode_list[i].get_size()<<endl;
	}
	textFs.close();
	return 0;
}
