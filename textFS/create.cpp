/*
 * create.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: swapnil
 */
#include <bits/stdc++.h>
#include "inode.h"
#include "superblock.h"
#include "settings.h"

using namespace std;

super_block sblock;
string NEW_FILE_NAME;

bool init()
{
	get_home_dir(); 												/* inits HOME_DIR variable */
	TEXT_FS = "/.textFS";
	TEXT_FS_PATH = HOME_DIR + TEXT_FS;

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	if(!textFs.good())
	{
		cout<<"No TextFs found... Creating new TextFs...\n";
		textFs.close();
		textFs.open(TEXT_FS_PATH.c_str(), ios::out | ios::binary);

		super_block super;
		textFs.write((char*)&super.first_inode_addr, sizeof(unsigned int));
		textFs.write((char*)&super.total_size, sizeof(unsigned int));

		if(textFs.good())
		{
			cout<<"TextFs created Successfully!!\n";
			textFs.close();
			return 1;
		}
		else
		{
			cout<<"TextFs creation failed :(\n";
			textFs.close();
			return 0;
		}
	}
	textFs.close();

	textFs.open(TEXT_FS_PATH.c_str(), ios::in | ios::binary);
	int total_size;
	textFs.seekg(sizeof(unsigned int));
	textFs.read((char*)&total_size, sizeof(unsigned int));
	sblock.total_size = total_size;
	textFs.close();
	return 1;
}

int main(int argc, char const *argv[])
{
	if(!init())
		return -1;

	if(argc < 2)
	{
		cout<<"Invalid no of arguments\n";
		cout<<"usage: create <filename>\n";
		return -1;
	}

	NEW_FILE_NAME = (string)argv[1];

	fstream textFs;
	textFs.open(TEXT_FS_PATH.c_str(), ios::binary | ios::in | ios::out);

	int file_name_length;
	char *file_name;
	int start_position;
	int file_size;
	int next_inode_addr;

	textFs.seekg(sblock.first_inode_addr);

	while(textFs.tellg() < sblock.total_size)
	{
		textFs.read((char*)&file_name_length, sizeof(int));

		file_name = new char[file_name_length + 1];
		textFs.read(file_name, file_name_length);
		file_name[file_name_length] = '\0';

		textFs.read((char*)&start_position, sizeof(int));
		textFs.read((char*)&file_size, sizeof(int));
		textFs.read((char*)&next_inode_addr, sizeof(int));

		if(string(file_name) == NEW_FILE_NAME)
		{
			cout<<"File already exists!!\n";
			return -1;
		}
		
		if(next_inode_addr == -1)
			break;

		textFs.seekg(next_inode_addr, ios_base::cur);              /* address where next inode starts.. 
																		next_inode_addr is offset 
																		from end of previous inode*/
	}

	if(textFs.tellg() > 3 * sizeof(unsigned int))				/* if there's already a inode */
	{
		long pos = textFs.tellp();
		textFs.seekp(pos - sizeof(int));						/* setting put pointer to one int location behind
																	to update next inode address */
		int updated_addr = sblock.total_size - pos;
		textFs.write((char*)&updated_addr, sizeof(int));
	}
	inode i(NEW_FILE_NAME);

	textFs.seekp(sblock.total_size);									/* maybe ambiguous */
	textFs.write((char*)&i.file_name_length, sizeof(int));
	textFs.write(i.file_name, i.file_name_length);
	textFs.write((char*)&i.start_position, sizeof(int));
	textFs.write((char*)&i.file_size, sizeof(int));
	textFs.write((char*)&i.next_inode_addr, sizeof(int));

	/* winding up */
	sblock.total_size += i.get_size();
	textFs.seekp(sizeof(unsigned int));
	textFs.write((char*)&sblock.total_size, sizeof(unsigned int));
	textFs.close();
}
