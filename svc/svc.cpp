/*
Operation no codes:
1 - list of all strings
2 - append
3 - delete


Format of command file
<operation_no> <list od ids> or
<operation_no> <id>
*/


#include <bits/stdc++.h>
typedef long long unsigned llu;
using namespace std;

class Version
{
public:
	Version(int a)
	{
		operation_no = a;
	}
	~Version(){}
	vector<int> ids;
	int operation_no; /* either 1, 2 or 3 */
};


vector<int> generate_version(int req, vector<Version> v)
{

	if(req < 0)
	{
		std::vector<int> v;
		return v;
	}
	int op_no = v[req].operation_no;
	if(op_no == 1)
	{
		return v[req].ids;
	}
	std::vector<int> ithla = generate_version(req - 1, v);
	
	if(op_no == 2)
	{
		ithla.push_back(v[req].ids[0]);
	}
	else
	{
		ithla.erase(ithla.begin() + v[req].ids[0]);
	}
	return ithla;
}



int main(int argc, char  *argv[])
{
	if(argc < 3)
	{
		cout<<"Invalid number of arguments\n";
		cout<<"    usage: svc <FILE_NAME> commit\nor  usage: svc <FILE_NAME> <VERSION_NO>\n";
	}
	if(argv[1] == NULL)
	{
		cerr<<"No file name\n    usage: svc <FILE_NAME> commit\nor  usage: svc <FILE_NAME> <VERSION_NO>\n";
		return -1;
	}
	ifstream in;
	in.open(argv[1]);
	if(!in.good())
	{
		cerr<<"Invalid file name\n";
		cout<<"    usage: svc <FILE_NAME> commit\nor  usage: svc <FILE_NAME> <VERSION_NO>\n";
		return -1;
	}

	string check = argv[2];
	if(check != "commit")
	{
		for (int i = 0; i < check.length(); ++i)
		{
			if(check[i] > '9' or check[i] < '0')
			{
				cout<<"Invalid command\n";
				return -1;
			}
		}
	}
	
/*Reading history of file using its svc file */

	int latest_version = 0, index = 0;
	string COMMAND_FILE = ".svc_comm_" + (string)argv[1];
	string CONTENT_FILE = ".svc_cont_" + (string)argv[1];
	fstream command_file, content_file;
	string line;
	string temp = "";
	std::vector<Version> version;

	command_file.open(COMMAND_FILE.c_str());
	content_file.open(CONTENT_FILE.c_str());

	if(!command_file.good() | !content_file.good())
		latest_version = -1;
	while(getline(command_file,line))
	{
		latest_version += 1;
		int operation_no = line[0] - '0';
		
		version.push_back(Version(operation_no));	
		temp = "";
		for (int i = 2; i < line.length(); ++i)
		{
			if(line[i] == ' ')
			{
				(version[index].ids).push_back(atoi(temp.c_str()));
				temp = "";
			}
			else
				temp += line[i];
		}
		index++;
	}
	if(latest_version > 0)
		latest_version -= 1;

	content_file.close();
	command_file.close();

	if((string)argv[2] == "commit")
	{
		std::vector<int> prev_ver = generate_version(latest_version, version);
		std::vector<int> current;
		std::map<int, string> itos;
		std::map<string, int> stoi;
		
		content_file.open(CONTENT_FILE.c_str());
		while(getline(content_file, line))
		{
			temp = "";
			int id;
			int i;
			for(i = 0;line[i] != ' ';i++)
			{
				temp += line[i];
			}
			id = atoi(temp.c_str());
			temp = "";
			i++;
			for (; i < line.length(); ++i)
			{
				temp += line[i];
			}
			itos[id] = temp;
			stoi[temp] = id;
		}
		content_file.close();
		content_file.open(CONTENT_FILE.c_str(), ios::app | ios::out);

		int deleted;
		int index = 0;
		bool flag = 0;
		while(getline(in, line))
		{
			if(stoi.find(line) == stoi.end())
			{
				int id;
				id = itos.size();
				itos[id] = line;
				stoi[line] = id;
				content_file<<id<<" "<<line<<endl;
			}
			current.push_back(stoi[line]);

			if(!flag and prev_ver.size() != 0 and index <= prev_ver.size()-1 and prev_ver[index] != current[index])
			{
				flag = 1;
				deleted = index;
			}
			index++;
		}
		content_file.close();

		command_file.open(COMMAND_FILE.c_str(), ios::app | ios::out);

		if((latest_version + 1)%10 == 0)
		{
			command_file<<"1 ";
			for (int i = 0; i < current.size(); ++i)
			{
				command_file<<current[i]<<" ";
			}
			command_file<<"\n";
		}
		else if(current.size() > prev_ver.size())
		{
			command_file<<"2 "<<current[current.size() - 1]<<" \n";
		}
		else
		{
			command_file<<"3 "<<deleted<<" \n";
		}
		command_file.close();
	}
	else
	{
		int requested_version = atoi(((string)argv[2]).c_str());
		if(latest_version == -1)
		{
			cout<<"Error : No previous commits found. Please commit the file first\n";
			return -1;
		}
		if(requested_version > latest_version)
		{
			cout<<"Error : Requested version cannot be displayed. Latest version is "<<latest_version<<"\n";
			return -1;
		}

		std::map<int, string> itos;
		content_file.open(CONTENT_FILE.c_str());
		while(getline(content_file, line))
		{
			temp = "";
			int id;
			int i;
			for(i = 0;line[i] != ' ';i++)
			{
				temp += line[i];
			}
			id = atoi(temp.c_str());
			temp = "";
			i++;
			for (; i < line.length(); ++i)
			{
				temp += line[i];
			}
			itos[id] = temp;
		}
		content_file.close();

		vector<int> v = generate_version(requested_version, version);
		for (int i = 0; i < v.size(); ++i)
		{
			cout<<itos[v[i]]<<"\n";
		}
	}
	in.close();
	command_file.close();
	content_file.close();
	return 0;
}