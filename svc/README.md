SVC (SIMPLE VERSION CONTROL)
======================================================


SVC is used to provide simple version control to a text file.
Only the changes regarding the file are stored instead of entire 
files. The required version is generated whenever required.



Modules:
-----------------------------------------------------------

1) Commit
2) Print any version


Usage info:
------------------------------------------------------------

The file should created before commiting.After the first commit
only two operations are permitted.
1. Appending a new line at the end of the file.
2. Deleting any existing line.

1) To commit a file

   svc <FILE_NAME> commit
   
2) To print a specific version
   
   svc <FILE_NAME> <VERSION_NO>
   
Authors:
---------------------------------------------------------
1) Pooja Patil
2) Mrinmayi Kinare
3) Swapnil Khandekar

Bug Report:
---------------------------------------------------------
You can submit your bugs here: poojavp95@gmail.com   
